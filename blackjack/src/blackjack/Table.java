package blackjack;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.Timer;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Class representing a table, where
 * players can play the game.
 * This class has all the methods
 * that control the actual flow of the game.
 */
@XmlAccessorType(XmlAccessType.NONE)
public class Table implements Serializable {
	private static final long serialVersionUID = 3934422413677103558L;

	private Deck deck = new Deck();
	@XmlElement
	private int decks;
	private Player dealer = new Player("dealer");
	private List<Player> players = new ArrayList<Player>();
	private List<Player> playing = new ArrayList<Player>();
	private Iterator<Player> iterator;
	private Player activePlayer;
	@XmlElement
	private String id;

	private boolean over = true;
	private boolean holeRevealed;

	private Timer newGameTimer;
	private Timer timeout;

	/**
	 * Constructor for Table.
	 * Timers are set here.
	 * newGameTimer attempts to start a new game
	 * 60 seconds after finishing the last one,
	 * timeout kicks inactive players after 60 seconds.
	 * @param id String
	 * @param decks int
	 */
	public Table(String id, int decks) {
		this.id = id;
		this.decks = decks;
		newGameTimer = new Timer(60000, ea -> {
			newGame();
		});

		timeout = new Timer(60000, ea -> {
			try {
				leave(activePlayer);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		});
	}

	/**
	 * Constructor for Table, used only by JAXB
	 */
	public Table() {
		newGameTimer = new Timer(60000, ea -> {
			newGame();
		});

		timeout = new Timer(60000, ea -> {
			try {
				leave(activePlayer);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		});
	}

	/**
	 * p hits.
	 * Gets a new card, his new point is calculated,
	 * and actions are taken accordingly: if he has
	 * more than 21 points, then he busts,
	 * if he has blackjack, then he stands.
	 * @param p Player
	 */
	public void hit(Player p) {
		if (p.isActive()) {
			timeout.restart();
			Player player = pickHand(p);
			try {
				player.drawCard(deck.draw());
				System.out.println(player.getid() + " hit.");
			} catch (EmptyDeckException e) {
				System.err.println("The deck is empty");
				e.printStackTrace();
			}
			if (player.getPoints() > 21) {
				bust(player);
				System.out.println(player.getid() + " busted.");
			}
			if (player.getPoints() == 21) {
				stand(player);
				System.out.println(player.getid() + " has blackjack.");
			}
		}
	}

	/**
	 * p stands.
	 * His turn ends.
	 * @param p Player
	 */
	public void stand(Player p) {
		if (p.isActive()) {
			timeout.restart();
			Player player = pickHand(p);
			System.out.println(player.getid() + " stood.");
			if (!checkSideHand(p)) {
				player.setActive(false);
				next();
			}
		}
	}

	/**
	 * p doubles.
	 * He gets exactly one more card and then
	 * he stands.
	 * @param p Player
	 * @throws InsufficientFundsException
	 */
	public void doubleDown(Player p) throws InsufficientFundsException {
		Player player = pickHand(p);
		if (player.isActive()) {
			timeout.restart();
			System.out.println(player.getid() + " doubled.");
			int bet = player.getBet();
			player.bet(bet);
			try {
				player.drawCard(deck.draw());
			} catch (EmptyDeckException e) {
				System.err.println("The deck is empty");
				e.printStackTrace();
			}
			if (!checkSideHand(player)) {
				player.setActive(false);
				next();
			}
		}
	}

	/**
	 * player splits.
	 * Only possible, if this is his first action,
	 * and he has 2 cards that are identical in value.
	 * His hand is split into two separate hands
	 * and a new card is dealt to each one.
	 * @param player Player
	 * @throws InsufficientFundsException
	 */
	public void split(Player player) throws InsufficientFundsException {
		if (player.isActive()
				&& player.getHand().size() == 2
				&& !player.playingSide()
				&& (player.getHand().get(0).getValue() == player.getHand()
						.get(1).getValue())) {
			timeout.restart();
			System.out.println(player.getid() + " split.");
			player.split(deck);
			if (player.getPoints() == 21) {
				stand(player);
				System.out.println(player.getid() + " has blackjack.");
			}
		}
	}

	/**
	 * player surrenders.
	 * Only possible, if this is his first action.
	 * Half his bet is returned to the player, then he busts.
	 * @param player Player
	 */
	public void surrender(Player player) {
		if (player.isActive() && player.getHand().size() == 2
				&& !player.playingSide()) {
			timeout.restart();
			System.out.println(player.getid() + " surrendered.");
			player.setActive(false);
			int bet = player.getBet();
			try {
				player.bet(-(bet / 2));
			} catch (InsufficientFundsException e) {
				e.printStackTrace();
			}
			bust(player);
			next();
		}
	}

	/**
	 * player puts bet amount of money in insurance.
	 * Only possible, if this is his first action,
	 * and the dealer's revealed card is an ace.
	 * player wins the insurance if the dealer's other
	 * card turns out to be of value 10.
	 * @param player Player
	 * @param bet int
	 * @throws InsufficientFundsException
	 */
	public void insurance(Player player, int bet)
			throws InsufficientFundsException {
		if (player.isActive() && player.getHand().size() == 2
				&& dealer.getHand().get(0).getRank() == CardRank.ACE
				&& player.getInsurance() == 0 && !player.playingSide()) {
			timeout.restart();
			System.out.println(player.getid() + " insured.");
			player.insurance(bet);
		}
	}

	/**
	 * p busts, meaning his hand loses.
	 * @param p Player
	 */
	public void bust(Player p) {
		p.setBusted(true);
		Player player = pickHand(p);
		if (!checkSideHand(player)) {
			next();
		}
	}

	/**
	 * The dealer's turn.
	 * Hits, until he has less then 17 points.
	 * Also hits on soft17, meaning he has 17 points,
	 * including at least one ace.
	 */
	public void dealer() {
		timeout.stop();
		holeRevealed = true;
		Deck dealerHand = dealer.getHand();
		Card c1 = dealerHand.get(0);
		Card c2 = dealerHand.get(1);
		boolean soft17 = ((c1.getRank() == CardRank.ACE && c2.getRank() == CardRank.SIX) || (c1
				.getRank() == CardRank.SIX && c2.getRank() == CardRank.ACE));
		while (dealer.getPoints() < 17 || (dealer.getPoints() == 17 && soft17)) {
			try {
				dealer.drawCard(deck.draw());
			} catch (EmptyDeckException e) {
				System.err.println("The deck is empty");
				e.printStackTrace();
			}
		}

		over();
	}

	/**
	 * A new game is started.
	 * State of the table is reset,
	 * list of playing players are set,
	 * deck is prepared, and cards are dealt.
	 */
	public void newGame() {
		if (over && players.size() != 0 && ready()) {
			newGameTimer.stop();
			resetTable();

			setPlaying();

			prepareDeck();

			System.out.println("New game started");

			deal();

			timeout.start();

			next();

		}
	}

	/**
	 * Deals two cards to everyone at the table.
	 */
	private void deal() {
		try {
			for (int i = 0; i < 2; i++) {
				for (Player player : players) {
					player.drawCard(deck.draw());
				}
				dealer.drawCard(deck.draw());
			}
		} catch (EmptyDeckException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Puts decks amount of decks into the main deck,
	 * then shuffles it.
	 */
	private void prepareDeck() {
		for (CardSuit cs : CardSuit.values())
			for (CardRank cn : CardRank.values())
				for (int i = 0; i < decks; i++)
					deck.add(new Card(cs, cn));

		deck.shuffle();
	}

	/**
	 * Sets the list of players who are going to play.
	 */
	private void setPlaying() {
		for (Player player : players) {
			player.setEvaluated(false);
			if (!player.isPlaying())
				leave(player);
			else
				playing.add(player);
		}

		iterator = playing.iterator();
	}

	/**
	 * Table is returned to its inital state,
	 * except for the list of joined players.
	 */
	private void resetTable() {
		over = false;
		holeRevealed = false;
		deck.clear();
		for (Player player : players) {
			player.throwAll();
			player.setBusted(false);
		}
		dealer.throwAll();

		playing.clear();
	}

	/**
	 * Evaluates players, returning the amount
	 * of money he won at the end of the game.
	 * @param player Player
	 * @return int
	 */
	public int evaluate(Player player) {
		newGameTimer.restart();
		player.setEvaluated(true);
		int wins = 0;

		wins = evaluateInsurance(player, wins);

		wins = evaluateSide(player, wins);

		wins = evaluateMain(player, wins);

		return wins;
	}

	/**
	 * Evaluates player's main hand
	 * @param player Player
	 * @param wins int
	 * @return int
	 */
	private int evaluateMain(Player player, int wins) {
		int mainBet = player.getBet();
		if (player.getPoints() == 21 && dealer.getPoints() == 21) {
			int bet = player.getBet();
			try {
				player.bet(-bet);
			} catch (InsufficientFundsException e) {
				e.printStackTrace();
			}
		} else if (player.getPoints() == 21) {
			wins += mainBet * 1.5;
			player.win(true);
		} else if (player.getPoints() > 21) {
			wins -= mainBet;
			player.lose();
		} else if (dealer.getPoints() == 21) {
			wins -= mainBet;
			player.lose();
		} else if (dealer.getPoints() > 21) {
			wins += mainBet;
			player.win(false);
		} else if (player.getPoints() > dealer.getPoints()
				&& !player.isBusted()) {
			wins += mainBet;
			player.win(false);
		} else {
			wins -= mainBet;
			player.lose();
		}
		return wins;
	}

	/**
	 * Evaluates the players side hand.
	 * @param player Player
	 * @param wins int
	 * @return int
	 */
	private int evaluateSide(Player player, int wins) {
		int sideBet = player.getSideHand().getBet();
		if (player.playingSide()) {
			Player side = player.getSideHand();
			if (side.getPoints() == 21 && dealer.getPoints() == 21) {
				int bet = side.getBet();
				try {
					side.bet(-bet);
				} catch (InsufficientFundsException e) {
					e.printStackTrace();
				}
			} else if (side.getPoints() == 21) {
				wins += sideBet;
				player.winSide();
			} else if (side.getPoints() > 21) {
				wins -= sideBet;
				player.loseSide();
			} else if (dealer.getPoints() == 21) {
				wins -= sideBet;
				player.loseSide();
			} else if (dealer.getPoints() > 21) {
				wins += sideBet;
				player.winSide();
			} else if (side.getPoints() > dealer.getPoints()) {
				wins += sideBet;
				player.winSide();
			} else {
				wins -= sideBet;
				player.loseSide();
			}
		}
		return wins;
	}

	/**
	 * Evaluates insurance.
	 * @param player Player
	 * @param wins int
	 * @return int
	 */
	private int evaluateInsurance(Player player, int wins) {
		int insurance = player.getInsurance();
		Deck dealerHand = dealer.getHand();
		if (dealerHand.get(0).getRank() == CardRank.ACE
				&& dealerHand.get(1).getValue() == 10) {
			wins += insurance * 2;
			player.winInsurance();
		} else {
			wins -= insurance;
			player.loseInsurance();
		}
		return wins;
	}

	/**
	 * Picks the next active player.
	 */
	public void next() {
		if (iterator.hasNext()) {
			Player next = iterator.next();
			if (!next.inSession() || !next.isPlaying())
				next();
			else {
				next.setActive(true);
				activePlayer = next;
				timeout.restart();
			}
		} else {
			dealer();
		}
	}

	/**
	 * Return true, if player has a side hand he hasn't played yet.
	 * @param player Player
	 * @return boolean
	 */
	public boolean checkSideHand(Player player) {
		if (!player.isSide()) {
			boolean playingSide = player.getSideHand().isPlaying()
					&& !player.getSideHand().isActive();
			if (playingSide) {
				player.getSideHand().setActive(true);
				if (player.getSideHand().getPoints() == 21) {
					stand(player);
				}
			}
			return playingSide;
		} else
			return false;
	}

	/**
	 * Method getDealerPoints.
	 * @return int
	 */
	public int getDealerPoints() {
		if (holeRevealed)
			return dealer.getPoints();
		else if (dealer.getHand().size() > 0)
			return dealer.getHand().get(0).getValue();
		else
			return 0;
	}

	/**
	 * Method getDealerCards.
	 * @return Deck
	 */
	public Deck getDealerCards() {
		return dealer.getHand();
	}

	/**
	 * Method isOver.
	 * @return boolean
	 */
	public boolean isOver() {
		return over;
	}

	/**
	 * player attempts to join the table.
	 * If it is full, TableFullException is thrown.
	 * @param player Player
	 * @throws TableFullException
	 */
	public void join(Player player) throws TableFullException {
		if (players.size() < 5) {
			player.setPlaying(false);
			player.setActive(false);
			players.add(player);
		} else
			throw (new TableFullException());
	}

	/**
	 * player leaves the table.
	 * @param player Player
	 */
	public void leave(Player player) {
		if (player.isActive()) {
			bust(player);
			if (player.playingSide()) {
				bust(player);
			}
		}
		player.setPlaying(false);
		player.closeSession();
		players.remove(player);
		newGame();
	}

	/**
	 * player stays at the table for the next game,
	 * with a bet amount of starting bet.
	 * @param player Player
	 * @param bet int
	 * @throws InsufficientFundsException
	 */
	public void stay(Player player, int bet) throws InsufficientFundsException {
		player.bet(bet);
		player.setPlaying(true);
		newGame();
	}

	/**
	 * Returns true, if all players are ready
	 * to start a new game.
	 * @return boolean
	 */
	public boolean ready() {
		boolean ready = true;
		for (Player player : players) {
			if (!player.isPlaying())
				ready = false;
		}
		return ready;
	}

	/**
	 * Method getPlayerIdList.
	 * @return String[]
	 */
	public String[] getPlayerIdList() {
		List<String> play = new ArrayList<String>();
		for (Player player : playing) {
			play.add(player.getid());
		}
		return play.toArray(new String[0]);
	}

	/**
	 * Ends the current game.
	 */
	public void over() {
		over = true;
		for (Player player : playing) {
			player.setPlaying(false);
		}
	}

	/**
	 * Method holeRevealed.
	 * @return boolean
	 */
	public boolean holeRevealed() {
		return holeRevealed;
	}

	/**
	 * Returns player's active hand.
	 * @param player Player
	 * @return Player
	 */
	public Player pickHand(Player player) {
		if (!player.sideActive())
			return player;
		else
			return player.getSideHand();
	}
	
	/**
	 * Method getActivePlayer.
	 * @return Player
	 */
	public Player getActivePlayer() {
		return activePlayer;
	}
}
