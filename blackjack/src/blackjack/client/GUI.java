package blackjack.client;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.rmi.RemoteException;
import java.util.Arrays;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.Timer;

import blackjack.CardRank;
import blackjack.Deck;
import blackjack.InsufficientFundsException;
import blackjack.server.ServerInterface;

/**
 * The graphical user interface.
 */
public class GUI extends JFrame {

	private static final long serialVersionUID = -1402252779303804232L;

	private JButton hit = new JButton("Hit");
	private JButton stand = new JButton("Stand");
	private JButton doubleDown = new JButton("Double");
	private JButton split = new JButton("Split");
	private JButton surrender = new JButton("Surrender");
	private JButton insurance = new JButton("Insurance");
	private GameArea gameArea = new GameArea(this);

	private volatile boolean over;

	protected ServerInterface server;
	protected String playerId;
	protected String tableId;
	protected BlackjackClient client;

	private Timer refreshTimer;
	private Timer timeout;

	private List<String> playing;

	/**
	 * Constructor for GUI.
	 * @param server ServerInterface
	 * @param playerId String
	 * @param tableId String
	 * @param client BlackjackClient
	 */
	GUI(ServerInterface server, String playerId, String tableId,
			BlackjackClient client) {
		this.server = server;
		this.playerId = playerId;
		this.tableId = tableId;
		this.client = client;

		refreshTimer = new Timer(100, ea -> {
			refresh();
		});
		refreshTimer.setRepeats(true);

		timeout = new Timer(60000, ea -> { 
				try {
					server.leave(playerId, tableId);
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			});

		refreshTimer.start();

		WindowListener exitListener = new WindowAdapter() {

			@Override
			public void windowClosing(WindowEvent e) {
				try {
					if (server.inSession(playerId)) {
						server.leave(playerId, tableId);
						System.out.println("closing");
					}
					System.exit(0);

				} catch (RemoteException e1) {
					e1.printStackTrace();
				}
			}
		};
		this.addWindowListener(exitListener);

		setTitle("BlackJack");
		setSize(600, 600);
		setLocation(500, 200);

		gameArea.setBackground(new Color(0, 200, 0));
		gameArea.setBorder(BorderFactory.createLineBorder(Color.black));

		JPanel buttons = new JPanel(new FlowLayout());

		buttons.add(hit);
		buttons.add(stand);
		buttons.add(doubleDown);
		buttons.add(split);
		buttons.add(surrender);
		buttons.add(insurance);

		this.add(gameArea, BorderLayout.CENTER);
		this.add(buttons, BorderLayout.SOUTH);

		hit.addActionListener(e -> {
			try {
				timeout.restart();
				server.hit(playerId, tableId);
				refresh();
			} catch (RemoteException e1) {
				System.err.println("Connection error");
				e1.printStackTrace();
			}
		});

		stand.addActionListener(e -> {
			try {
				timeout.stop();
				timeout.setDelay(10000);
				server.stand(playerId, tableId);
				refresh();
			} catch (RemoteException e1) {
				System.err.println("Connection error");
				e1.printStackTrace();
			}
		});

		split.addActionListener(e -> {
			try {
				timeout.stop();
				timeout.setDelay(10000);
				server.split(playerId, tableId);
				refresh();
			} catch (RemoteException e1) {
				System.err.println("Connection error");
				e1.printStackTrace();
			} catch (InsufficientFundsException e1) {
				JOptionPane.showConfirmDialog(null,
						"You don't have enough money to do that.", "BlackJack",
						JOptionPane.DEFAULT_OPTION);
			}
		});

		doubleDown.addActionListener(e -> {
			try {
				timeout.stop();
				timeout.setDelay(10000);
				server.doubleDown(playerId, tableId);
				refresh();
			} catch (RemoteException e1) {
				System.err.println("Connection error");
				e1.printStackTrace();
			} catch (InsufficientFundsException e1) {
				JOptionPane.showConfirmDialog(null,
						"You don't have enough money to do that.", "BlackJack",
						JOptionPane.DEFAULT_OPTION);
			}
		});

		surrender.addActionListener(e -> {
			try {
				timeout.stop();
				timeout.setDelay(10000);
				server.surrender(playerId, tableId);
				refresh();
			} catch (RemoteException e1) {
				System.err.println("Connection error");
				e1.printStackTrace();
			}
		});

		insurance.addActionListener(e -> {
			try {
				timeout.restart();
				int bet = BlackjackClient.placeBet();
				server.insurance(playerId, tableId, bet);
				refresh();
			} catch (RemoteException e1) {
				System.err.println("Connection error");
				e1.printStackTrace();
			} catch (InsufficientFundsException e1) {
				JOptionPane.showConfirmDialog(null,
						"You don't have enough money to do that.", "BlackJack",
						JOptionPane.DEFAULT_OPTION);
			}
		});

		setVisible(true);
	}

	/**
	 * Refreshes the GUI.
	 */
	public void refresh() {
		try {
			if (!server.inSession(playerId)) {
				JOptionPane.showConfirmDialog(null, "Your session ended.",
						"BlackJack", JOptionPane.DEFAULT_OPTION);
				System.exit(0);
			} else {
				gameArea.repaint();
				if (!over) {
					inProgress();
				} else {
					gameOver();
				}
			}
		} catch (RemoteException e) {
			System.err.println("Connection error");
			e.printStackTrace();
		}
	}

	/**
	 * Sets the GUI if game is still in progress.
	 * @throws RemoteException
	 */
	private void inProgress() throws RemoteException {
		over = server.isOver(tableId);

		if (over || !server.isActive(playerId)
				|| !playing.contains(playerId)) {
			justFinished();
		} else {
			inGame();
		}
		if (server.isActive(playerId)) {
			timeout.start();
		}
	}

	/**
	 * Sets the GUI if the game if over.
	 * @throws RemoteException
	 */
	private void gameOver() throws RemoteException {
		if (server.getPlayerPoints(playerId) == 21
				&& !server.isEvaluated(playerId)) {
			disableAll();
			refreshTimer.stop();
			client.evaluate(tableId);
		}

		newGame();
	}

	/**
	 * Sets the GUI if the game just finished.
	 * @throws RemoteException
	 */
	private void justFinished() throws RemoteException {
		disableAll();

		if (playing != null && playing.contains(playerId) && over) {
			refreshTimer.stop();
			client.evaluate(tableId);
		}
	}

	/**
	 * Sets the GUI if it still hasn't finished.
	 * @throws RemoteException
	 */
	private void inGame() throws RemoteException {
		hit.setEnabled(true);
		stand.setEnabled(true);
		doubleDown.setEnabled(true);
		if (server.getPlayerCards(playerId).size() == 2) {
			checkSurrender();
			Deck dealer = server.getDealerCards(tableId);
			Deck player = server.getPlayerCards(playerId);
			checkInsurance(dealer);
			checkSplit(player);
		} else {
			doubleDown.setEnabled(false);
			split.setEnabled(false);
			surrender.setEnabled(false);
			insurance.setEnabled(false);
		}
		checkForBlackjack();
	}
	
	/**
	 * Checks if player has blackjack.
	 * If he does, the player stands.
	 */
	private void checkForBlackjack() throws RemoteException {
		if (server.getPlayerPoints(playerId) == 21 && 
				!server.isPlayingSide(playerId))
			server.stand(playerId, tableId);
	}

	/**
	 * Checks, if split is possible.
	 * @param player Deck
	 * @throws RemoteException
	 */
	private void checkSplit(Deck player) throws RemoteException {
		if (!server.isPlayingSide(playerId)
				&& player.get(0).getValue() == player
						.get(1).getValue())
			split.setEnabled(true);
		else
			split.setEnabled(false);
	}

	/**
	 * Checks, if insurance is possible.
	 * @param dealer Deck
	 * @throws RemoteException
	 */
	private void checkInsurance(Deck dealer) throws RemoteException {
		if (dealer.get(0).getRank() == CardRank.ACE
				&& server.getInsurance(playerId) == 0) {
			insurance.setEnabled(true);
		} else
			insurance.setEnabled(false);
	}

	/**
	 * Checks, if surrender is possible.
	 * @throws RemoteException
	 */
	private void checkSurrender() throws RemoteException {
		if (!server.isPlayingSide(playerId))
			surrender.setEnabled(true);
		else
			surrender.setEnabled(false);
	}

	/**
	 * Disables everything.
	 */
	private void disableAll() {
		hit.setEnabled(false);
		stand.setEnabled(false);
		doubleDown.setEnabled(false);
		surrender.setEnabled(false);
		insurance.setEnabled(false);
	}

	/**
	 * Sets up the GUI for a new game.
	 */
	public void newGame() {
		try {
			over = server.isOver(tableId);
			refreshTimer.restart();
			playing = Arrays.asList(server.getPlayerList(tableId));
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}
}
