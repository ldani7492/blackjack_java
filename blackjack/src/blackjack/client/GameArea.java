package blackjack.client;

import java.awt.Color;
import java.awt.Graphics;
import java.rmi.RemoteException;
import java.util.Arrays;
import java.util.List;

import javax.swing.JPanel;

import blackjack.Card;
import blackjack.Deck;

/**
 * The graphical part of the user interface,
 * where the cards are drawn.
 */
class GameArea extends JPanel {

	private static final long serialVersionUID = 5060593748000544515L;

	private final GUI gui;

	private List<String> players;

	/**
	 * Constructor for GameArea.
	 * @param gui GUI
	 */
	public GameArea(GUI gui) {
		this.gui = gui;
	}

	/**
	 * Method paintComponent.
	 * @param g Graphics
	 */
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		g.setColor(Color.WHITE);

		setPlayerList();

		try {
			int i = 0;
			i = drawPlayerCards(g, i);

			drawDealerCards(g, i);

			g.setColor(Color.WHITE);
			String ppoints = String.valueOf(gui.server.getPlayerPoints(
					gui.playerId));
			String dpoints = String.valueOf(gui.server
					.getDealerPoints(gui.tableId));
			if (gui.server.isPlayingSide(gui.playerId)) {
				int sidepoints = gui.server.getSidePoints(gui.playerId);
				g.drawString("Player points: " + ppoints + " Dealer points: "
						+ dpoints + " Side hand points: " + sidepoints, 20, 400);
			} else
				g.drawString("Player points: " + ppoints + " Dealer points: "
						+ dpoints, 20, 400);
			String money = String.valueOf(gui.server.getMoney(gui.playerId));
			String bet = String.valueOf(gui.server.getBet(gui.playerId));
			String insurance = String.valueOf(gui.server
					.getInsurance(gui.playerId));
			g.drawString("Money: " + money, 20, 420);
			g.drawString("Bet: " + bet, 20, 440);
			g.drawString("Insurance: " + insurance, 20, 460);
			if (gui.server.isPlayingSide(gui.playerId)) {
				String sideBet = String.valueOf(gui.server
						.getSideBet(gui.playerId));
				g.drawString("Side bet: " + sideBet, 20, 480);
			}

		} catch (RemoteException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Draws the dealer's cards into line i.
	 * @param g Graphics
	 * @param i int
	 * @throws RemoteException
	 */
	private void drawDealerCards(Graphics g, int i) throws RemoteException {
		int j;
		j = 0;
		g.setColor(Color.WHITE);
		g.drawString("Dealer's cards: ", 20, 20 + 75 * i);
		Deck dealer = this.gui.server.getDealerCards(this.gui.tableId);
		if (gui.server.holeRevealed(gui.tableId)) {
			for (Card card : dealer) {
				drawCard(card, i, j, g);
				j++;
			}
		} else if (dealer.size() != 0) {
			Card card = dealer.get(0);
			drawCard(card, i, j, g);
			j++;
			g.setColor(Color.BLACK);
			g.fillRect(20 + 32 * j, 30 + 75 * i, 30, 40);
			g.setColor(Color.WHITE);
			g.drawString("hole", 25 + 32 * j, 50 + 75 * i);
		}
	}

	/**
	 * Draws the player's cards into line i.
	 * @param g Graphics
	 * @param i int
	 * @return int
	 * @throws RemoteException
	 */
	private int drawPlayerCards(Graphics g, int i) throws RemoteException {
		int j = 0;

		for (String playerId : players) {
			j = 0;
			g.setColor(Color.WHITE);
			g.drawString(playerId + "'s cards: ", 20, 20 + 75 * i);
			for (Card card : gui.server.getPlayerCards(playerId)) {
				drawCard(card, i, j, g);
				j++;
			}
			i++;
			if (gui.server.isPlayingSide(playerId)) {
				i--;
				j = 8;
				for (Card card : gui.server.getSideCards(playerId)) {
					drawCard(card, i, j, g);
					j++;
				}
				i++;
			}

		}
		return i;
	}

	/**
	 * Draws card to the coordinate i, j.
	 * @param card Card
	 * @param i int
	 * @param j int
	 * @param g Graphics
	 */
	public void drawCard(Card card, int i, int j, Graphics g) {
		g.setColor(Color.WHITE);
		g.fillRect(20 + 32 * j, 30 + 75 * i, 30, 40);
		g.setColor(Color.BLACK);
		g.drawRect(20 + 32 * j, 30 + 75 * i, 30, 40);
		g.setColor(card.getColor());
		g.drawString(card.toString(), 25 + 32 * j, 50 + 75 * i);
	}

	/**
	 * Loads the list of players in-game.
	 */
	public void setPlayerList() {
		try {
			players = Arrays.asList(gui.server.getPlayerList(gui.tableId));
		} catch (RemoteException e) {
			System.out.println("Failed to get player list");
			e.printStackTrace();
		}
	}
}