package blackjack.client;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import javax.swing.JOptionPane;

import blackjack.InsufficientFundsException;
import blackjack.TableFullException;
import blackjack.server.ServerInterface;

/**
 * The implementation of the client.
 */
public class BlackjackClient {

	private String playerId;
	private String tableId;
	private static BlackjackClient client;
	private static String address;
	private ServerInterface server;
	private GUI gui;

	/**
	 * Constructor for BlackjackClient.
	 * Connects to the server and attempts to start a new game.
	 * @throws RemoteException
	 * @throws NotBoundException
	 */
	protected BlackjackClient() throws RemoteException, NotBoundException {
		String name = "BlackjackServer";
		Registry registry = LocateRegistry.getRegistry(address); // RMIRegistry
		// c�me
		server = (ServerInterface) registry.lookup(name);
		System.out.println("connected to " + address);

		String[] tables = server.getTableList();

		playerId = JOptionPane.showInputDialog(null, "Please enter your name:");

		if (playerId == null) {
			System.exit(0);
		}

		if (server.inSession(playerId)) {
			JOptionPane.showConfirmDialog(null,
					"Can't join: you already have an active session.",
					"BlackJack", JOptionPane.DEFAULT_OPTION);
			System.exit(0);
		}

		boolean success;

		do {
			tableId = (String) JOptionPane.showInputDialog(null,
					"Please choose a table:", "Tables",
					JOptionPane.QUESTION_MESSAGE, null, tables, tables[0]);

			if (tableId == null) {
				System.exit(0);
			}

			try {
				server.join(playerId, tableId);
				System.out.println("Joined " + tableId + " as " + playerId);
				success = true;
			} catch (TableFullException e) {
				JOptionPane.showConfirmDialog(null,
						"Table is full. Please choose a different one",
						"BlackJack", JOptionPane.DEFAULT_OPTION);
				System.err.println("Table is full");
				success = false;
			}
		} while (!success);
	}

	/**
	 * Method getInstance.
	 * @return BlackjackClient
	 * @throws RemoteException
	 * @throws NotBoundException
	 */
	public static BlackjackClient getInstance() throws RemoteException,
			NotBoundException {
		if (client == null) {
			client = new BlackjackClient();
		}
		return client;
	}

	/**
	 * Method main.
	 * The GUI is initialized and a staring bet is placed.
	 * @param args String[]
	 */
	public static void main(String args[]) {
		try {
			address = args[0];

			client = new BlackjackClient();

			client.setGUI(new GUI(client.getServer(), client.getPlayerId(),
					client.getTableId(), getInstance()));

			int bet = placeBet();
			try {
				newGame(bet);
			} catch (InsufficientFundsException e) {
				JOptionPane.showConfirmDialog(null,
						"You don't have enough money to do that.", "BlackJack",
						JOptionPane.DEFAULT_OPTION);
				System.err.println("Insufficient funds");
				client.getServer().leave(client.getPlayerId(),
						client.getTableId());
				System.exit(0);
			}

		} catch (RemoteException e) {
			System.err.println("Connection error");
			e.printStackTrace();
		} catch (NotBoundException e) {
			System.err.println("Couldn't connect to server");
			e.printStackTrace();
		}
	}

	/**
	 * Method evaluate.
	 * Money won is shown, and the player can decide if he wants to play again.
	 * @param tableId String
	 * @throws RemoteException
	 */
	public void evaluate(String tableId) throws RemoteException {
		int win = server.evaluate(playerId, tableId);
		int newGame = JOptionPane.NO_OPTION;

		if (server.inSession(playerId)) {

			newGame = JOptionPane.showConfirmDialog(null, "You won " + win
					+ " amount of money. Would you like to start a new game?",
					"BlackJack", JOptionPane.YES_NO_OPTION);

		} else {
			newGame = JOptionPane.NO_OPTION;
			JOptionPane.showConfirmDialog(null, "Your session ended.",
					"BlackJack", JOptionPane.DEFAULT_OPTION);
		}

		if (newGame == JOptionPane.YES_OPTION) {
			int bet = placeBet();
			try {
				newGame(bet);
			} catch (InsufficientFundsException e) {
				JOptionPane.showConfirmDialog(null,
						"You don't have enough money to do that.", "BlackJack",
						JOptionPane.DEFAULT_OPTION);
				System.err.println("Insufficient funds");
				server.leave(playerId, tableId);
				System.exit(0);
			} catch (NotBoundException e) {
				e.printStackTrace();
			}
		} else {
			server.leave(playerId, tableId);
			gui.setVisible(false);
			gui.dispose();
			System.out.println("closing");
		}
	}

	/**
	 * Signals to the server and the gui that
	 * the player wants to start a new game.
	 * @param bet int
	 * @throws RemoteException
	 * @throws InsufficientFundsException
	 * @throws NotBoundException
	 */
	public static void newGame(int bet) throws RemoteException,
			InsufficientFundsException, NotBoundException {
		BlackjackClient client = getInstance();
		client.getServer().stay(client.getPlayerId(), client.getTableId(), bet);
		client.getGUI().newGame();
	}

	/**
	 * Asks the user how much bet he would like to bet.
	 * @return int
	 */
	public static int placeBet() {
		int bet;
		try {
			String betString = JOptionPane.showInputDialog(null,
					"Please place your bet:");
			if (betString == null) {
				System.exit(0);
			}
			bet = Integer.parseInt(betString);
			while (bet <= 0) {
				betString = JOptionPane.showInputDialog(null,
						"Please place your bet:");
				if (betString == null) {
					System.exit(0);
				}
				bet = Integer.parseInt(betString);
			}
		} catch (NumberFormatException e) {
			System.err.println("Wrong number format");
			return placeBet();
		}
		return bet;
	}

	/**
	 * Method getPlayerId.
	 * @return String
	 */
	public String getPlayerId() {
		return playerId;
	}

	/**
	 * Method getTableId.
	 * @return String
	 */
	public String getTableId() {
		return tableId;
	}

	/**
	 * Method getServer.
	 * @return ServerInterface
	 */
	public ServerInterface getServer() {
		return server;
	}

	/**
	 * Method getGUI.
	 * @return GUI
	 */
	public GUI getGUI() {
		return gui;
	}

	/**
	 * Method setGUI.
	 * @param gui GUI
	 */
	public void setGUI(GUI gui) {
		this.gui = gui;
	}
}
