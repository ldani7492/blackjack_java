package blackjack.server;

import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;

import blackjack.Player;

/**
 * Helper class fog JAXB.
 */
@XmlRootElement
public class Players {
	private Map<String, Player> players = new HashMap<String, Player>();

	/**
	 * Method getPlayers.
	 * @return Map<String,Player>
	 */
	public Map<String, Player> getPlayers() {
		return players;
	}

	/**
	 * Method setPlayers.
	 * @param players Map<String,Player>
	 */
	public void setPlayers(Map<String, Player> players) {
		this.players = players;
	}
}
