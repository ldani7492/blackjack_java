package blackjack.server;

import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;

import blackjack.Table;

/**
 * Loads table information from XML.
 */
@XmlRootElement
public class Tables {
	private Map<String, Table> tables = new HashMap<String, Table>();

	/**
	 * Method getTables.
	 * @return Map<String,Table>
	 */
	public Map<String, Table> getTables() {
		return tables;
	}

	/**
	 * Method setTables.
	 * @param tables Map<String,Table>
	 */
	public void setTables(Map<String, Table> tables) {
		this.tables = tables;
	}
}
