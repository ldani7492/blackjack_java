package blackjack.server;

import java.io.File;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.Map;

import javax.swing.Timer;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import blackjack.Deck;
import blackjack.InsufficientFundsException;
import blackjack.Player;
import blackjack.Table;
import blackjack.TableFullException;

/**
 * The implementation of the server.
 */
public class BlackjackServer implements ServerInterface {

	private Map<String, Table> tables;
	private Map<String, Player> players;
	private Timer saveTimer;

	/**
	 * Constructor for BlackjackServer. XML data is loaded
	 * and timer is set.
	 */
	public BlackjackServer() {
		super();

		readTables();

		readPlayers();

		saveTimer = new Timer(60000, ea -> {
			savePlayers();
		});

		saveTimer.start();
		saveTimer.setRepeats(true);
	}

	/**
	 * Loads table information from XML.
	 */
	private void readTables() {
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(Tables.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			Tables tableMap = (Tables) jaxbUnmarshaller.unmarshal(new File(
					"src/blackjack/server/tables.xml"));

			tables = tableMap.getTables();
			System.out.println("Tables loaded");

		} catch (JAXBException e) {
			System.err.println("Error loading tables");
			e.printStackTrace();
		}
	}

	/**
	 * Loads player information from XML.
	 */
	private void readPlayers() {
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(Players.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			Players playerMap = (Players) jaxbUnmarshaller.unmarshal(new File(
					"src/blackjack/server/players.xml"));

			players = playerMap.getPlayers();
			System.out.println("Players loaded");

		} catch (JAXBException e) {
			System.err.println("Couldn't load players, creating new database");
			players = new HashMap<String, Player>();
		}
	}

	/**
	 * Saves player information to XML.
	 */
	private void savePlayers() {
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(Players.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

			Players playerMap = new Players();
			playerMap.setPlayers(players);

			jaxbMarshaller.marshal(playerMap, new File(
					"src/blackjack/server/players.xml"));
			System.out.println("Players saved");

		} catch (JAXBException e) {
			System.err.println("Error saving players");
			e.printStackTrace();
		}
	}

	/**
	 * Method main.
	 * The server side of RMI is set up.
	 * @param args String[]
	 */
	public static void main(String[] args) {

		try {
			String name = "BlackjackServer";
			ServerInterface server = new BlackjackServer();
			ServerInterface stub = (ServerInterface) UnicastRemoteObject
					.exportObject(server, 0);
			Registry registry = LocateRegistry.createRegistry(1099);
			registry.rebind(name, stub);
			System.out.println("BlackjackServer started");
		} catch (RemoteException e) {
			System.err.println("Could not start server");
			e.printStackTrace();
		}
	}

	/**
	 * Method hit.
	 * @param playerId String
	 * @param tableId String
	 * @throws RemoteException
	 * @see blackjack.server.ServerInterface#hit(String, String)
	 */
	public void hit(String playerId, String tableId) throws RemoteException {
		Player player = players.get(playerId);
		Table table = tables.get(tableId);
		table.hit(player);
	}

	/**
	 * Method stand.
	 * @param playerId String
	 * @param tableId String
	 * @throws RemoteException
	 * @see blackjack.server.ServerInterface#stand(String, String)
	 */
	public void stand(String playerId, String tableId) throws RemoteException {
		Player player = players.get(playerId);
		Table table = tables.get(tableId);
		table.stand(player);

	}

	/**
	 * Method doubleDown.
	 * @param playerId String
	 * @param tableId String
	 * @throws RemoteException
	 * @throws InsufficientFundsException
	 * @see blackjack.server.ServerInterface#doubleDown(String, String)
	 */
	public void doubleDown(String playerId, String tableId)
			throws RemoteException, InsufficientFundsException {
		Table table = tables.get(tableId);
		Player player = players.get(playerId);
		table.doubleDown(player);
	}

	/**
	 * Method split.
	 * @param playerId String
	 * @param tableId String
	 * @throws RemoteException
	 * @throws InsufficientFundsException
	 * @see blackjack.server.ServerInterface#split(String, String)
	 */
	public void split(String playerId, String tableId) throws RemoteException,
			InsufficientFundsException {
		Table table = tables.get(tableId);
		Player player = players.get(playerId);
		table.split(player);
	}

	/**
	 * Method surrender.
	 * @param playerId String
	 * @param tableId String
	 * @throws RemoteException
	 * @see blackjack.server.ServerInterface#surrender(String, String)
	 */
	public void surrender(String playerId, String tableId)
			throws RemoteException {
		Table table = tables.get(tableId);
		Player player = players.get(playerId);
		table.surrender(player);
	}

	/**
	 * Method insurance.
	 * @param playerId String
	 * @param tableId String
	 * @param bet int
	 * @throws RemoteException
	 * @throws InsufficientFundsException
	 * @see blackjack.server.ServerInterface#insurance(String, String, int)
	 */
	public void insurance(String playerId, String tableId, int bet)
			throws RemoteException, InsufficientFundsException {
		Table table = tables.get(tableId);
		Player player = players.get(playerId);
		table.insurance(player, bet);
	}

	/**
	 * Method join.
	 * @param playerId String
	 * @param tableId String
	 * @return boolean
	 * @throws RemoteException
	 * @throws TableFullException
	 * @see blackjack.server.ServerInterface#join(String, String)
	 */
	public boolean join(String playerId, String tableId)
			throws RemoteException, TableFullException {
		if (!players.containsKey(playerId)) {
			players.put(playerId, new Player(playerId));
		}

		Player player = players.get(playerId);
		if (player.inSession())
			return false;

		Table table = tables.get(tableId);
		player.reset();
		table.join(player);
		player.startSession();
		return true;

	}

	/**
	 * Method evaluate.
	 * @param playerId String
	 * @param tableId String
	 * @return int
	 * @see blackjack.server.ServerInterface#evaluate(String, String)
	 */
	public int evaluate(String playerId, String tableId) {
		Player player = players.get(playerId);
		Table table = tables.get(tableId);
		return table.evaluate(player);
	}

	/**
	 * Method leave.
	 * @param playerId String
	 * @param tableId String
	 * @throws RemoteException
	 * @see blackjack.server.ServerInterface#leave(String, String)
	 */
	public void leave(String playerId, String tableId) throws RemoteException {
		Player player = players.get(playerId);
		Table table = tables.get(tableId);
		table.leave(player);
		player.closeSession();
	}

	/**
	 * Method stay.
	 * @param playerId String
	 * @param tableId String
	 * @param bet int
	 * @throws RemoteException
	 * @throws InsufficientFundsException
	 * @see blackjack.server.ServerInterface#stay(String, String, int)
	 */
	public void stay(String playerId, String tableId, int bet)
			throws RemoteException, InsufficientFundsException {
		Player player = players.get(playerId);
		Table table = tables.get(tableId);
		table.stay(player, bet);
	}

	/**
	 * Method getPlayerPoints.
	 * @param playerId String
	 * @return int
	 * @throws RemoteException
	 * @see blackjack.server.ServerInterface#getPlayerPoints(String)
	 */
	public int getPlayerPoints(String playerId) throws RemoteException {
		Player player = players.get(playerId);
			return player.getPoints();
	}

	/**
	 * Method getDealerPoints.
	 * @param id String
	 * @return int
	 * @throws RemoteException
	 * @see blackjack.server.ServerInterface#getDealerPoints(String)
	 */
	public int getDealerPoints(String id) throws RemoteException {
		Table table = tables.get(id);
		return table.getDealerPoints();
	}

	/**
	 * Method getPlayerCards.
	 * @param playerId String
	 * @return Deck
	 * @throws RemoteException
	 * @see blackjack.server.ServerInterface#getPlayerCards(String)
	 */
	public Deck getPlayerCards(String playerId) throws RemoteException {
		Player player = players.get(playerId);
		return player.getHand();
	}

	/**
	 * Method getDealerCards.
	 * @param id String
	 * @return Deck
	 * @throws RemoteException
	 * @see blackjack.server.ServerInterface#getDealerCards(String)
	 */
	public Deck getDealerCards(String id) throws RemoteException {
		Table table = tables.get(id);
		return table.getDealerCards();
	}

	/**
	 * Method inSession.
	 * @param id String
	 * @return boolean
	 * @throws RemoteException
	 * @see blackjack.server.ServerInterface#inSession(String)
	 */
	public boolean inSession(String id) throws RemoteException {
		if (!players.containsKey(id)) {
			return false;
		}
		Player player = players.get(id);
		return player.inSession();
	}

	/**
	 * Method isOver.
	 * @param id String
	 * @return boolean
	 * @throws RemoteException
	 * @see blackjack.server.ServerInterface#isOver(String)
	 */
	public boolean isOver(String id) throws RemoteException {
		Table table = tables.get(id);
		return table.isOver();
	}

	/**
	 * Method getTableList.
	 * @return String[]
	 * @throws RemoteException
	 * @see blackjack.server.ServerInterface#getTableList()
	 */
	public String[] getTableList() throws RemoteException {
		return tables.keySet().toArray(new String[0]);
	}

	/**
	 * Method isPlaying.
	 * @param playerId String
	 * @return boolean
	 * @throws RemoteException
	 * @see blackjack.server.ServerInterface#isPlaying(String)
	 */
	public boolean isPlaying(String playerId) throws RemoteException {
		Player player = players.get(playerId);
		return player.isPlaying();
	}

	/**
	 * Method getPlayerList.
	 * @param tableId String
	 * @return String[]
	 * @throws RemoteException
	 * @see blackjack.server.ServerInterface#getPlayerList(String)
	 */
	public String[] getPlayerList(String tableId) throws RemoteException {
		Table table = tables.get(tableId);
		return table.getPlayerIdList();
	}

	/**
	 * Method isActive.
	 * @param playerId String
	 * @return boolean
	 * @throws RemoteException
	 * @see blackjack.server.ServerInterface#isActive(String)
	 */
	public boolean isActive(String playerId) throws RemoteException {
		Player player = players.get(playerId);
		return player.isActive();
	}

	/**
	 * Method holeRevealed.
	 * @param tableId String
	 * @return boolean
	 * @throws RemoteException
	 * @see blackjack.server.ServerInterface#holeRevealed(String)
	 */
	public boolean holeRevealed(String tableId) throws RemoteException {
		Table table = tables.get(tableId);
		return table.holeRevealed();
	}

	/**
	 * Method isEvaluated.
	 * @param playerId String
	 * @return boolean
	 * @throws RemoteException
	 * @see blackjack.server.ServerInterface#isEvaluated(String)
	 */
	public boolean isEvaluated(String playerId) throws RemoteException {
		Player player = players.get(playerId);
		return player.isEvaluated();
	}

	/**
	 * Method getBet.
	 * @param playerId String
	 * @return int
	 * @throws RemoteException
	 * @see blackjack.server.ServerInterface#getBet(String)
	 */
	public int getBet(String playerId) throws RemoteException {
		Player player = players.get(playerId);
		return player.getBet();
	}

	/**
	 * Method getMoney.
	 * @param playerId String
	 * @return int
	 * @throws RemoteException
	 * @see blackjack.server.ServerInterface#getMoney(String)
	 */
	public int getMoney(String playerId) throws RemoteException {
		Player player = players.get(playerId);
		return player.getMoney();
	}

	/**
	 * Method getInsurance.
	 * @param playerId String
	 * @return int
	 * @throws RemoteException
	 * @see blackjack.server.ServerInterface#getInsurance(String)
	 */
	public int getInsurance(String playerId) throws RemoteException {
		Player player = players.get(playerId);
		return player.getInsurance();
	}

	/**
	 * Method isPlayingSide.
	 * @param playerId String
	 * @return boolean
	 * @throws RemoteException
	 * @see blackjack.server.ServerInterface#isPlayingSide(String)
	 */
	public boolean isPlayingSide(String playerId) throws RemoteException {
		Player player = players.get(playerId);
		return player.playingSide();
	}

	/**
	 * Method getSideBet.
	 * @param playerId String
	 * @return int
	 * @throws RemoteException
	 * @see blackjack.server.ServerInterface#getSideBet(String)
	 */
	public int getSideBet(String playerId) throws RemoteException {
		Player player = players.get(playerId);
		return player.getSideHand().getBet();
	}

	/**
	 * Method getSidePoints.
	 * @param playerId String
	 * @return int
	 * @throws RemoteException
	 * @see blackjack.server.ServerInterface#getSidePoints(String)
	 */
	public int getSidePoints(String playerId) throws RemoteException {
		Player player = players.get(playerId);
		return player.getSideHand().getPoints();
	}

	/**
	 * Method getSideCards.
	 * @param playerId String
	 * @return Deck
	 * @throws RemoteException
	 * @see blackjack.server.ServerInterface#getSideCards(String)
	 */
	public Deck getSideCards(String playerId) throws RemoteException {
		Player player = players.get(playerId);
		return player.getSideHand().getHand();

	}

}
