package blackjack.server;

import java.rmi.Remote;
import java.rmi.RemoteException;

import blackjack.Deck;
import blackjack.InsufficientFundsException;
import blackjack.TableFullException;

/**
 * The interface for RMI.
 */
public interface ServerInterface extends Remote {
	/**
	 * Method hit.
	 * @param playerId String
	 * @param tableId String
	 * @throws RemoteException
	 */
	public void hit(String playerId, String tableId) throws RemoteException;

	/**
	 * Method stand.
	 * @param playerId String
	 * @param tableId String
	 * @throws RemoteException
	 */
	public void stand(String playerId, String tableId) throws RemoteException;

	/**
	 * Method doubleDown.
	 * @param playerId String
	 * @param tableId String
	 * @throws RemoteException
	 * @throws InsufficientFundsException
	 */
	public void doubleDown(String playerId, String tableId)
			throws RemoteException, InsufficientFundsException;

	/**
	 * Method split.
	 * @param playerId String
	 * @param tableId String
	 * @throws RemoteException
	 * @throws InsufficientFundsException
	 */
	public void split(String playerId, String tableId) throws RemoteException,
			InsufficientFundsException;

	/**
	 * Method insurance.
	 * @param playerId String
	 * @param tableId String
	 * @param bet int
	 * @throws RemoteException
	 * @throws InsufficientFundsException
	 */
	public void insurance(String playerId, String tableId, int bet)
			throws RemoteException, InsufficientFundsException;

	/**
	 * Method surrender.
	 * @param playerId String
	 * @param tableId String
	 * @throws RemoteException
	 */
	public void surrender(String playerId, String tableId)
			throws RemoteException;
	
	/**
	 * Method join.
	 * @param playerId String
	 * @param tableId String
	 * @return boolean
	 * @throws RemoteException
	 * @throws TableFullException
	 */
	public boolean join(String playerId, String tableId)
			throws RemoteException, TableFullException;

	/**
	 * Method leave.
	 * @param playerId String
	 * @param tableId String
	 * @throws RemoteException
	 */
	public void leave(String playerId, String tableId) throws RemoteException;

	/**
	 * Method stay.
	 * @param playerId String
	 * @param tableId String
	 * @param bet int
	 * @throws RemoteException
	 * @throws InsufficientFundsException
	 */
	public void stay(String playerId, String tableId, int bet)
			throws RemoteException, InsufficientFundsException;

	/**
	 * Method getPlayerPoints.
	 * @param playerId String
	 * @return int
	 * @throws RemoteException
	 */
	public int getPlayerPoints(String playerId) throws RemoteException;

	/**
	 * Method getDealerPoints.
	 * @param id String
	 * @return int
	 * @throws RemoteException
	 */
	public int getDealerPoints(String id) throws RemoteException;

	/**
	 * Method getPlayerCards.
	 * @param playerId String
	 * @return Deck
	 * @throws RemoteException
	 */
	public Deck getPlayerCards(String playerId) throws RemoteException;

	/**
	 * Method getDealerCards.
	 * @param id String
	 * @return Deck
	 * @throws RemoteException
	 */
	public Deck getDealerCards(String id) throws RemoteException;

	/**
	 * Method inSession.
	 * @param id String
	 * @return boolean
	 * @throws RemoteException
	 */
	public boolean inSession(String id) throws RemoteException;

	/**
	 * Method isOver.
	 * @param id String
	 * @return boolean
	 * @throws RemoteException
	 */
	public boolean isOver(String id) throws RemoteException;

	/**
	 * Method evaluate.
	 * @param playerId String
	 * @param tableId String
	 * @return int
	 * @throws RemoteException
	 */
	public int evaluate(String playerId, String tableId) throws RemoteException;

	/**
	 * Method getTableList.
	 * @return String[]
	 * @throws RemoteException
	 */
	public String[] getTableList() throws RemoteException;

	/**
	 * Method isPlaying.
	 * @param playerId String
	 * @return boolean
	 * @throws RemoteException
	 */
	public boolean isPlaying(String playerId) throws RemoteException;

	/**
	 * Method getPlayerList.
	 * @param tableId String
	 * @return String[]
	 * @throws RemoteException
	 */
	public String[] getPlayerList(String tableId) throws RemoteException;

	/**
	 * Method isActive.
	 * @param playerId String
	 * @return boolean
	 * @throws RemoteException
	 */
	public boolean isActive(String playerId) throws RemoteException;

	/**
	 * Method holeRevealed.
	 * @param tableId String
	 * @return boolean
	 * @throws RemoteException
	 */
	public boolean holeRevealed(String tableId) throws RemoteException;

	/**
	 * Method isEvaluated.
	 * @param playerId String
	 * @return boolean
	 * @throws RemoteException
	 */
	public boolean isEvaluated(String playerId) throws RemoteException;

	/**
	 * Method getBet.
	 * @param playerId String
	 * @return int
	 * @throws RemoteException
	 */
	public int getBet(String playerId) throws RemoteException;

	/**
	 * Method getMoney.
	 * @param playerId String
	 * @return int
	 * @throws RemoteException
	 */
	public int getMoney(String playerId) throws RemoteException;

	/**
	 * Method getInsurance.
	 * @param playerId String
	 * @return int
	 * @throws RemoteException
	 */
	public int getInsurance(String playerId) throws RemoteException;

	/**
	 * Method isPlayingSide.
	 * @param playerId String
	 * @return boolean
	 * @throws RemoteException
	 */
	public boolean isPlayingSide(String playerId) throws RemoteException;

	/**
	 * Method getSideBet.
	 * @param playerId String
	 * @return int
	 * @throws RemoteException
	 */
	public int getSideBet(String playerId) throws RemoteException;

	/**
	 * Method getSidePoints.
	 * @param playerId String
	 * @return int
	 * @throws RemoteException
	 */
	public int getSidePoints(String playerId) throws RemoteException;

	/**
	 * Method getSideCards.
	 * @param playerId String
	 * @return Deck
	 * @throws RemoteException
	 */
	public Deck getSideCards(String playerId) throws RemoteException;

}
