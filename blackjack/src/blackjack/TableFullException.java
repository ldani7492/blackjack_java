package blackjack;

/**
 * Exception, that is thrown, when a player attempts
 * to join a table that is already full.
 */
public class TableFullException extends Exception {
	private static final long serialVersionUID = 2198486905917339081L;
}