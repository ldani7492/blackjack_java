package blackjack;

/**
 * Exception, which is thrown, when a player attempts to bet more money
 * than the amount he owns.
 */
public class InsufficientFundsException extends Exception {
	private static final long serialVersionUID = -4309737451300332642L;

}
