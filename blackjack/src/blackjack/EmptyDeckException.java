package blackjack;

/**
 * Exception, which is thrown, when we are trying to draw
 * from an empty deck.
 */
public class EmptyDeckException extends Exception {
	private static final long serialVersionUID = 5351533983288721711L;
}