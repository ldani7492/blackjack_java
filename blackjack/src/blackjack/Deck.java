package blackjack;

import java.util.*;

/**
 * Implementation of a deck containing cards
 */
public class Deck extends ArrayList<Card> {

	private static final long serialVersionUID = 5540832176342824311L;

	/**
	 * Draws a card from the deck.
	 * If the deck is not empty, it returns the drawn cards
	 * and removes it from the deck. Otherwise it throws
	 * an EmptyDeckException.
	 * @return Card
	 * @throws EmptyDeckException
	 */
	public Card draw() throws EmptyDeckException {

		if (size() == 0)
			throw (new EmptyDeckException());
		return remove(0);
	}

	/**
	 * Shuffles the cards in the deck.
	 */
	public void shuffle() {
		Collections.shuffle(this);
	}

	/**
	 * The String representation of a deck.
	 * It consists of the String representations of
	 * the cards it contains, separated by whitespace.
	 * @return String
	 */
	public String toString() {
		StringBuilder cards = new StringBuilder();
		for (Card card : this) {
			cards.append(card.toString());
			cards.append(' ');
		}

		return cards.toString();
	}
}
