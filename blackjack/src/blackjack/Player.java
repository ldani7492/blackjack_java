package blackjack;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Class representing a player.
 */
@XmlAccessorType(XmlAccessType.NONE)
public class Player implements Serializable {

	private static final long serialVersionUID = 6556267046527282211L;

	private Deck hand = new Deck(); 
	private int points; 
	private int aces; 
	@XmlElement
	private String id;

	private boolean inSession;
	private boolean active;
	private boolean playing;
	private boolean evaluated;
	private boolean isSide;
	private boolean busted;

	@XmlElement
	private int money;
	private int bet;
	private int insurance;

	private Player sideHand;

	/**
	 * Constructor for Player.
	 * @param playerid String
	 */
	public Player(String playerid) {
		id = playerid;
		money = 1000;
		sideHand = new Player("side", true);
	}

	/**
	 * Constructor for Player.
	 * Only used for creating a side hand.
	 * @param playerid String
	 * @param isSide boolean
	 */
	public Player(String playerid, boolean isSide) {
		id = playerid;
		this.isSide = isSide;
	}

	/**
	 * Constructor for Player.
	 * Only used for JAXB.
	 */
	public Player() {
		sideHand = new Player("side", true);
	}

	/**
	 * Returns the player's current points.
	 * @return int
	 */
	public int getPoints() { 
		return points;
	}

	/**
	 * Throws away every card from both the player's hand
	 * and its side hand.
	 */
	public void throwAll() { 
		hand.clear();
		if (!isSide) {
			sideHand.throwAll();
			sideHand.setPlaying(false);
		}
		points = 0;
		aces = 0;
	}
	
	/**
	 * Sets everything apart from money to it's initial value.
	 */
	public void reset() {
		throwAll();
		bet = 0;
		insurance = 0;
		inSession = false;
		active = false;
		playing = false;
		evaluated = false;
		busted = false;
		if (!isSide)
			sideHand.reset();
	}

	/**
	 * Adds c to the player's hand, increments aces if it's an ace,
	 * and sets points accordingly.
	 * @param c Card
	 */
	public void drawCard(Card c) {
		hand.add(c); 
		if (c.getRank() == CardRank.ACE)
			aces++;
		points += c.getValue();

		while (points > 21 && aces > 0) { 
			points -= 10;
			aces--; 
		}
	}

	// A birtokolt lapokat adja vissza
	/**
	 * The String representation of the player.
	 * It returns it's main hand's String representation.
	 * @return String
	 */
	public String toString() {
		return hand.toString();
	}

	/**
	 * Returns the player's Id.
	 * @return String
	 */
	public String getid() {
		return id;
	}

	/**
	 * Returns the player's main hand.
	 * @return Deck
	 */
	public Deck getHand() {
		return hand;
	}

	/**
	 * Returns the player's side hand.
	 * @return Player
	 */
	public Player getSideHand() {
		return sideHand;
	}

	/**
	 * True, if the player is currently in session.
	 * @return boolean
	 */
	public boolean inSession() {
		return inSession;
	}

	/**
	 * True, if the player is currently active.
	 * @return boolean
	 */
	public boolean isActive() {
		return active;
	}

	/**
	 * Method setActive.
	 * @param a boolean
	 */
	public void setActive(boolean a) {
		active = a;
	}

	/**
	 * Method isPlaying.
	 * @return boolean
	 */
	public boolean isPlaying() {
		return playing;
	}

	/**
	 * Method setPlaying.
	 * @param p boolean
	 */
	public void setPlaying(boolean p) {
		playing = p;
	}

	/**
	 * Starts a new session for the player
	 */
	public void startSession() {
		inSession = true;
	}

	/**
	 * Ends the player's current session.
	 */
	public void closeSession() {
		inSession = false;
	}

	/**
	 * Method getMoney.
	 * @return int
	 */
	public int getMoney() {
		return money;
	}

	/**
	 * Method getBet.
	 * @return int
	 */
	public int getBet() {
		return bet;
	}

	/**
	 * Method getInsurance.
	 * @return int
	 */
	public int getInsurance() {
		return insurance;
	}

	/**
	 * Attempts to bet the given amount of money.
	 * Throws InsufficientFundsException if the player doesn't have
	 * enough money to do that.
	 * @param amount int
	 * @throws InsufficientFundsException
	 */
	public void bet(int amount) throws InsufficientFundsException {
		if (amount > money)
			throw new InsufficientFundsException();
		bet += amount;
		money -= amount;
	}

	/**
	 * Attempts to bet the given amount of money on a side bet.
	 * Throws InsufficientFundsException if the player doesn't have
	 * enough money to do that.
	 * @throws InsufficientFundsException
	 */
	public void sideBet() throws InsufficientFundsException {
		if (bet > money)
			throw new InsufficientFundsException();
		sideHand.setMoney(bet);
		sideHand.bet(bet);
		money -= bet;
	}

	/**
	 * Attempts to bet the given amount of money on insurance.
	 * Throws InsufficientFundsException if the player doesn't have
	 * enough money to do that.
	 * @param amount int
	 * @throws InsufficientFundsException
	 */
	public void insurance(int amount) throws InsufficientFundsException {
		if (amount > money || amount > bet * 1.5)
			throw new InsufficientFundsException();
		insurance += amount;
		money -= amount;
	}

	public void lose() {
		bet = 0;
	}

	/**
	 * Called when the player wins, money is given to the player
	 * according to the amount of bet he had.
	 * @param blackjack boolean
	 */
	public void win(boolean blackjack) {
		if (blackjack)
			money += bet * 2.5;
		else
			money += bet * 2;
		bet = 0;
	}

	/**
	 * Called when insurance is lost.
	 */
	public void loseInsurance() {
		insurance = 0;
	}

	/**
	 * Called when insurance is won.
	 */
	public void winInsurance() {
		money += bet * 3;
		bet = 0;
	}

	/**
	 * Called when the side hand loses.
	 */
	public void loseSide() {
		sideHand.lose();
	}

	/**
	 * Called when the side hand wins.
	 */
	public void winSide() {
		int bet = sideHand.getBet();
		sideHand.lose();
		money += bet * 2;
	}

	/**
	 * Method isEvaluated.
	 * @return boolean
	 */
	public boolean isEvaluated() {
		return evaluated;
	}

	/**
	 * Method setEvaluated.
	 * @param evaluated boolean
	 */
	public void setEvaluated(boolean evaluated) {
		this.evaluated = evaluated;
	}

	/**
	 * Splits the deck between the main and the side hand.
	 * Only callable when the player has only 2 cards,
	 * and they are of the same value. A new bet has to be
	 * put as a side bet in order to do this.
	 * @param deck Deck
	 * @throws InsufficientFundsException
	 */
	public void split(Deck deck) throws InsufficientFundsException {
		try {
			sideBet();
			Card card = hand.draw();
			if (card.getRank() == CardRank.ACE) {
				aces--;
				points--;
			} else
				points -= card.getValue();
			sideHand.drawCard(card);
			drawCard(deck.draw());
			sideHand.drawCard(deck.draw());
			sideHand.setPlaying(true);
		} catch (EmptyDeckException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Method setMoney.
	 * @param amount int
	 */
	public void setMoney(int amount) {
		money = amount;
	}

	/**
	 * Method playingSide.
	 * @return boolean
	 */
	public boolean playingSide() {
		return (!isSide) ? sideHand.isPlaying() : false;
	}

	/**
	 * Method sideActive.
	 * @return boolean
	 */
	public boolean sideActive() {
		return (!isSide) ? sideHand.isActive() : false;
	}

	/**
	 * Method isSide.
	 * @return boolean
	 */
	public boolean isSide() {
		return isSide;
	}

	/**
	 * Method isBusted.
	 * @return boolean
	 */
	public boolean isBusted() {
		return busted;
	}

	/**
	 * Method setBusted.
	 * @param busted boolean
	 */
	public void setBusted(boolean busted) {
		this.busted = busted;
	}

}
