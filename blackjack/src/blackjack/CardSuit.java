package blackjack;

import java.awt.Color;

/**
 * Enumeration for a card's suit.
 */
public enum CardSuit {

	SPADES('\u2660'), HEARTS('\u2665'), DIAMONDS('\u2666'), CLUBS('\u2663');

	public final char suit;

	/**
	 * Constructor for CardSuit.
	 * @param su char
	 */
	private CardSuit(char su) {
		suit = su;
	}

	/**
	 * Returns the given suit's color.
	 * @return Color
	 */
	public Color getColor() {
		if (this == HEARTS || this == DIAMONDS)
			return Color.RED;
		else
			return Color.BLACK;
	}
}