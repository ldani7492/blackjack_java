package blackjack;

import java.awt.Color;
import java.io.Serializable;

/**
 * The implementation of a single card
 */
public class Card implements Serializable {

	private static final long serialVersionUID = -1231107613261828543L;

	private CardSuit suit; 
	private CardRank number; 

	/**
	 * Constructor for Card.
	 * @param cs CardSuit
	 * @param cn CardNumber
	 */
	public Card(CardSuit cs, CardRank cn) {
		suit = cs;
		number = cn;
	}

	/**
	 * Returns the card's suit.
	 * @return CardSuit
	 */
	public CardSuit getSuit() { 
		return suit;
	}

	/**
	 * Returns the card's rank.
	 * @return CardRank
	 */
	public CardRank getRank() { 
		return number;
	}

	/**
	 * Returns the card's value.
	 * @return int
	 */
	public int getValue() { 
		return number.value;
	}

	/**
	 * Returns the card's color.
	 * @return Color
	 */
	public Color getColor() {
		return suit.getColor();
	}

	/**
	 * Returns the String representation of the card.
	 * This representation consists of its suit's symbol and its rank.
	 * @return String
	 */
	public String toString() { 
		return suit.suit + number.rank;
	}
}
