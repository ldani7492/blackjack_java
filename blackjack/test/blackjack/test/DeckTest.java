package blackjack.test;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import blackjack.Card;
import blackjack.CardRank;
import blackjack.CardSuit;
import blackjack.Deck;
import blackjack.EmptyDeckException;

/**
 */
public class DeckTest {

	private Deck deck;
	private Deck hand;

	@Before
	public void setUp() {
		deck = new Deck();
		for (CardSuit cs : CardSuit.values())
			for (CardRank cn : CardRank.values())
				deck.add(new Card(cs, cn));
		deck.shuffle();
		hand = new Deck();
	}

	/**
	 * Method testDraw.
	 * @throws EmptyDeckException
	 */
	@Test
	public void testDraw() throws EmptyDeckException {
		deck.draw();
		Assert.assertEquals(51, deck.size());
	}

	/**
	 * Method testEmptyDraw.
	 * @throws EmptyDeckException
	 */
	@Test(expected = EmptyDeckException.class)
	public void testEmptyDraw() throws EmptyDeckException {
		hand.draw();
	}

	@Test
	public void testClear() {
		deck.clear();
		Assert.assertEquals(0, deck.size());
	}

	@Test
	public void testList() {
		Card c1 = new Card(CardSuit.HEARTS, CardRank.KING);
		Card c2 = new Card(CardSuit.SPADES, CardRank.ACE);
		Card c3 = new Card(CardSuit.HEARTS, CardRank.QUEEN);
		hand.add(c1);
		hand.add(c2);
		hand.add(c3);
		Assert.assertEquals(
				c1.toString() + ' ' + c2.toString() + ' ' + c3.toString() + ' ',
				hand.toString());
	}

}
