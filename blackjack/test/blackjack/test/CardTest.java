package blackjack.test;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import blackjack.Card;
import blackjack.CardRank;
import blackjack.CardSuit;

/**
 */
@RunWith(Parameterized.class)
public class CardTest {

	private CardSuit cs;
	private CardRank cn;
	private Card card;

	/**
	 * Constructor for CardTest.
	 * @param cs CardSuit
	 * @param cn CardNumber
	 */
	public CardTest(CardSuit cs, CardRank cn) {
		this.cs = cs;
		this.cn = cn;
	}

	@Before
	public void setUp() {
		card = new Card(cs, cn);
	}

	@Test
	public void testCard() {
		Assert.assertEquals(cs, card.getSuit());
		Assert.assertEquals(cn, card.getRank());
		Assert.assertEquals(cn.value, card.getValue());
		Assert.assertEquals(cs.suit + cn.rank, card.toString());
	}

	/**
	 * Method parameters.
	 * @return List<Object[]>
	 */
	@Parameters
	public static List<Object[]> parameters() {
		List<Object[]> params = new ArrayList<Object[]>();
		for (CardSuit cs : CardSuit.values())
			for (CardRank cn : CardRank.values())
				params.add(new Object[] { cs, cn });
		return params;
	}

}
