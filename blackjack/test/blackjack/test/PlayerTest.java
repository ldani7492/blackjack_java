package blackjack.test;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import blackjack.Card;
import blackjack.CardRank;
import blackjack.CardSuit;
import blackjack.Player;

/**
 */
public class PlayerTest {

	private Player player;
	private Card c1;
	private Card c2;

	@Before
	public void setUp() {
		player = new Player("player");

		c1 = new Card(CardSuit.CLUBS, CardRank.TEN);
		c2 = new Card(CardSuit.CLUBS, CardRank.EIGHT);

		player.drawCard(c1);
		player.drawCard(c2);
	}

	@Test
	public void testDeck() {
		player.drawCard(new Card(CardSuit.SPADES, CardRank.TWO));
		Assert.assertEquals(20, player.getPoints());

		player.drawCard(new Card(CardSuit.SPADES, CardRank.THREE));
		Assert.assertEquals(23, player.getPoints());

		player.throwAll();
		Assert.assertEquals(0, player.getPoints());

		player.drawCard(new Card(CardSuit.CLUBS, CardRank.TWO));
		player.drawCard(new Card(CardSuit.SPADES, CardRank.ACE));
		Assert.assertEquals(13, player.getPoints());

		player.drawCard(new Card(CardSuit.SPADES, CardRank.ACE));
		Assert.assertEquals(14, player.getPoints());
	}

	@Test
	public void testList() {
		Assert.assertEquals(c1.toString() + ' ' + c2.toString() + ' ',
				player.toString());
	}

}
