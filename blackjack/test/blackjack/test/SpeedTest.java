package blackjack.test;

import blackjack.InsufficientFundsException;
import blackjack.Player;
import blackjack.Table;
import blackjack.TableFullException;

public class SpeedTest {
	public static void main(String args[]) {
		double startTime = System.currentTimeMillis();
		
		for (int i=0; i < 1000; i++) {
			play();
		}
		
		double stopTime = System.currentTimeMillis();
		double elapsedTime = stopTime - startTime;
		
		System.out.println(elapsedTime);
	}
	
	public static void play() {
		Table table = new Table("TestTable", 6);
		Player player1 = new Player("Player 1");
		Player player2 = new Player("Player 2");
		Player player3 = new Player("Player 3");
		
		player1.startSession();
		player2.startSession();
		player3.startSession();

		try {
			table.join(player1);
			table.join(player2);
			table.join(player3);
			
			table.stay(player1, 100);
			table.stay(player2, 100);
			table.stay(player3, 100);
			
			table.newGame();
			
			while (!table.isOver()) {
				table.stand(table.getActivePlayer());
			}

		} catch (TableFullException e) {
			e.printStackTrace();
		} catch (InsufficientFundsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
